#CUDA - FMPC 
Implementation of QPgen based modified IJS solver on GPU

Usage: 
1. mkdir build
2. cd build 
3. cmake .. 
4. make 
5. ./fmpc-cuda
