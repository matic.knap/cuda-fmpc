/*****************************************************************************/
/*** simple-server.c                                                       ***/
/***                                                                       ***/
/*****************************************************************************/

#include "QPgen.h"
#include "data_struct.h"
#include "work_space_struct.h"
#include <arpa/inet.h>
#include <errno.h>
#include <errno.h>
#include <fcntl.h> // for open
#include <resolv.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h> // for close
#include <cublas_v2.h>

#define MY_PORT 9912
#define MAXBUF 10000

void init_data(struct DATA *);
void init_work_space(struct WORK_SPACE *);
void free_data(struct DATA *);
void free_work_space(struct WORK_SPACE *);
void qp_nvidia(struct DATA *, struct WORK_SPACE *, double *, int *, double *);

/* App for testing purposes */
void print(FILE *file, double *arr, int length)
{
    int i = 0;
    for (i; i < length; ++i) {
        fprintf(file, "%f ", arr[i]);
    }
    fprintf(file, "\n");
}

void testCublasFramework();

int main(int argc, char **argv)
{

    // testCublasFramework();
    // return;

    int mfkdsja = 0;
    int sockfd;
    struct sockaddr_in self;

    struct timespec t1, t2;
    double elapsedTime = 0.0;

    /*---Create streaming socket---*/
    if ((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror("Socket");
        exit(errno);
    }

    /*---Initialize address/port structure---*/
    bzero(&self, sizeof(self));
    self.sin_family = AF_INET;
    self.sin_port = htons(MY_PORT);
    self.sin_addr.s_addr = INADDR_ANY;

    /*---Assign a port number to the socket---*/
    if (bind(sockfd, (struct sockaddr *)&self, sizeof(self)) != 0) {
        perror("socket--bind");
        exit(errno);
    }

    /*---Make it a "listening socket"---*/
    if (listen(sockfd, 20) != 0) {
        perror("socket--listen");
        exit(errno);
    }

    /*---Forever... ---*/
    int i = 0, j = 0;
    char *pEnd, *pStart;
    char *buffer;
    buffer = (char *)calloc(MAXBUF, sizeof(char));

    // allocate workspace and data space
    struct DATA *d = (struct DATA *)calloc(1, sizeof(struct DATA));
    //    cudaMalloc(&d, sizeof(struct DATA));
    init_data(d);

    struct WORK_SPACE *ws = (struct WORK_SPACE *)malloc(sizeof(struct WORK_SPACE));
    init_work_space(ws);

    /* nbr of iterations, size of output, size of input  */
    int iter;
    // TODO: read that from structures
    //    int sz = d->R->n;
    //    int size_input = d->R->m;

    int sz = 1893;
    int size_input = 33;

    static double *x;
    x = (double *)calloc(sz, sizeof(double));

    double *cudaBt;
    cudaMalloc(&cudaBt, sizeof(double)*62);

    // lock memory to RAM
    //    mlockall(MCL_CURRENT);
    while (1) {
        int clientfd;
        struct sockaddr_in client_addr;
        int addrlen = sizeof(client_addr);
        /*---accept a connection (creating a data pipe)---*/
        clientfd = accept(sockfd, (struct sockaddr *)&client_addr, (socklen_t *)&addrlen);
        printf(">>>>>>> %s:%d connected\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

        /* recieve data into buffer */
        int a = recv(clientfd, buffer, MAXBUF, 0);
        if (a <= 0) {
            perror("Receiving data. ");
            exit(errno);
        }
        printf(">>>>>>> Received %zd bytes.\n", a);

        i = j = 0;

        /** Clear workspace variables
         * @bug If variables ws->lambda, v, tmp_var_n_orig are not cleared it
         * affects solution
         * calculations. lambda and v at number of iterations and tmp_var_n_orig
         * at precision.!
         */
//        memset(ws->lambda, 0x0, d->E->n * sizeof(double));
//        memset(ws->v, 0x0, d->E->n * sizeof(double));
//        memset(ws->tmp_var_n_orig, 0x0, sz * sizeof(double));

        int fd = open("/dev/cpu_dma_latency", O_WRONLY);
        write(fd, 0, 1);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &t1);
        
        /* execute QPgen solver */
        cudaMemcpy(cudaBt,(double *)buffer,  sizeof(double)*62, cudaMemcpyHostToDevice);
        qp_nvidia(d, ws, x, &iter, (double *)cudaBt);
        
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &t2);
        close(fd);
        elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_nsec - t1.tv_nsec) / 1000000.0;

        /* pack data for sending back to MATLAB*/
        int xsend = send(clientfd, (char *)(&x[1860]), 11 * sizeof(double), 0);
        if (!xsend) {
            perror("Error sending data : x\n");
            exit(errno);
        }
        usleep(500);
        xsend = send(clientfd, (char *)&iter, sizeof(int), 0);
        if (!xsend) {
            perror("Error sending data : iter\n");
            exit(errno);
        }
        usleep(500);
        xsend = send(clientfd, (char *)&elapsedTime, sizeof(double), 0);
        if (!xsend) {
            perror("Error sending data : elapsedTime\n");
            exit(errno);
        }

        close(clientfd);
    }
    free(buffer);
    close(sockfd);
    return 0;
}


void testCublasFramework()
{
    int N = 5;
    int M = 3;
    int i = 0;

    double * matrix = (double *)calloc(M*N, sizeof(double));
    for (i = 0; i < M*N; i++)
        matrix[i] = i;

    double * vector = (double *)calloc(M, sizeof(double));
    for (i = 0; i < M; i++)
        vector[i] = 1;

    double * result = (double *)calloc(N, sizeof(double));
    for (i = 0; i < N; i++)
        result[i] = i*2;


    double *matrixCuda, *vectorCuda, *resultCuda;
    cudaMalloc(&matrixCuda, sizeof(double)*M*N);
    cudaMalloc(&vectorCuda, sizeof(double)*M);
    cudaMalloc(&resultCuda, sizeof(double)*N);

    cudaMemcpy((void *)matrixCuda, matrix, M*N*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy((void *)vectorCuda, vector, M*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy((void *)resultCuda, result, N*sizeof(double), cudaMemcpyHostToDevice);

    cublasHandle_t handle;
    cublasCreate(&handle);
    double alpha = 1.0;
    double beta = 0.0;
    cublasDgemv(handle, CUBLAS_OP_T, M, N, &alpha, matrixCuda, M, vectorCuda, 1, &beta,
                             resultCuda, 1);

    cudaMemcpy(result, resultCuda, N*sizeof(double), cudaMemcpyDeviceToHost);
    // cudaThreadSynchronize();
    for (i = 0; i < N; i++)
        printf("%f  ", result[i]);


    cublasDestroy(handle);
}








































