#include "QPgen.h"
#include "data_struct.h"

#include "work_space_struct.h"
#include <cublas_v2.h>
#include <math.h>

// TEMP:
#include <stdio.h>
#define DEBUG

#define blockSize 226
#define gridSize 1

/**
 * @brief      Diagonal matrix vector multiplication
 *
 * @param      a     { parameter_description }
 * @param      b     { parameter_description }
 * @param      c     { parameter_description }
 * @param[in]  n     { parameter_description }
 */
__global__ void mat_vect_mult_diag_cuda(double *a, double *b, double *c, int n)
{
    // Get our global thread ID
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    // Make sure we do not go out of bounds
    if (id < n)
        c[id] = a[id] * b[id];
}

__global__ static void clip_soft_cuda(double *x, double *l, double *u, double *soft, int len)
{
    // Get our global thread ID
    int jj = blockIdx.x * blockDim.x + threadIdx.x;
    if (jj < len) {

        if (soft[jj] < 0) {
            x[jj] = min(max(x[jj], l[jj]), u[jj]);
        }
        else if (x[jj] <= l[jj] - soft[jj]) {
            x[jj] = x[jj] + soft[jj];
        }
        else if (x[jj] >= u[jj] + soft[jj]) {
            x[jj] = x[jj] - soft[jj];
        }
        else {
            x[jj] = min(max(x[jj], l[jj]), u[jj]);
        }
    }
}


/**
 * @brief      Helper function for printing statuses
 *
 * @param[in]  error  The error
 *
 * @return     { description_of_the_return_value }
 */
static const char *_cudaGetErrorEnum(cublasStatus_t error)
{
    switch (error) {
    case CUBLAS_STATUS_SUCCESS:
        return "CUBLAS_STATUS_SUCCESS";

    case CUBLAS_STATUS_NOT_INITIALIZED:
        return "CUBLAS_STATUS_NOT_INITIALIZED";

    case CUBLAS_STATUS_ALLOC_FAILED:
        return "CUBLAS_STATUS_ALLOC_FAILED";

    case CUBLAS_STATUS_INVALID_VALUE:
        return "CUBLAS_STATUS_INVALID_VALUE";

    case CUBLAS_STATUS_ARCH_MISMATCH:
        return "CUBLAS_STATUS_ARCH_MISMATCH";

    case CUBLAS_STATUS_MAPPING_ERROR:
        return "CUBLAS_STATUS_MAPPING_ERROR";

    case CUBLAS_STATUS_EXECUTION_FAILED:
        return "CUBLAS_STATUS_EXECUTION_FAILED";

    case CUBLAS_STATUS_INTERNAL_ERROR:
        return "CUBLAS_STATUS_INTERNAL_ERROR";
    }

    return "<unknown>";
}

static void restart(cublasHandle_t *handle, double *x, double *x_old, double *y, double *y_old, double *tmp_var_p,
                    double *tmp_var_p2, int n)
{
    double test;
    cublasStatus_t status;
    double alpha = 1.0;

    // vec_sub(y_old, x, tmp_var_p, n);
    cudaMemset((void *)tmp_var_p, 0, n * sizeof(double));
    alpha = -1;
    status = cublasDaxpy(*handle, n, &alpha, x, 1, tmp_var_p, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    alpha = 1;
    status = cublasDaxpy(*handle, n, &alpha, y_old, 1, tmp_var_p, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    // vec_sub(x, x_old, tmp_var_p2, n);
    cudaMemset((void *)tmp_var_p2, 0, n * sizeof(double));
    alpha = -1;
    status = cublasDaxpy(*handle, n, &alpha, x_old, 1, tmp_var_p2, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    alpha = 1;
    status = cublasDaxpy(*handle, n, &alpha, x, 1, tmp_var_p2, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    //
    //     test = scalar_prod(tmp_var_p, tmp_var_p2, n);
    status = cublasDdot(*handle, n, tmp_var_p, 1, tmp_var_p2, 1, &test);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    if (test > 0) {
        //         copy_vec_part(x_old, y, n);
        status = cublasDcopy(*handle, n, (double *)x_old, 1, y, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));
        // copy_vec_part(x_old, x, n);
        status = cublasDcopy(*handle, n, (double *)x_old, 1, x, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));
    }
}


static double check_stop_cond_FGM(cublasHandle_t *handle, struct DIAG_MAT *Einv, double *y, double *v,
                                  double *tmp_var_p, double *tmp_var_p2, int p, double tol)
{
    double cond_num;
    double cond_den;
    double cond;
    cublasStatus_t status;
    double alpha = 1.0;

    // vec_sub(y, v, tmp_var_p, p);
    cudaMemset((void *)tmp_var_p, 0, p * sizeof(double));
    alpha = -1;
    status = cublasDaxpy(*handle, 226, &alpha, v, 1, tmp_var_p, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    alpha = 1;
    status = cublasDaxpy(*handle, 226, &alpha, y, 1, tmp_var_p, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    // mat_vec_mult_diag(Einv, tmp_var_p, tmp_var_p2);
    mat_vect_mult_diag_cuda<<<1, Einv->n>>>(Einv->data, tmp_var_p, tmp_var_p2, Einv->n);
    cudaThreadSynchronize();

    // cond_num = norm_sq(tmp_var_p2, p);
    status = cublasDnrm2(*handle, p, tmp_var_p2, 1, &cond_num);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    // mat_vec_mult_diag(Einv, y, tmp_var_p);
    mat_vect_mult_diag_cuda<<<1, Einv->n>>>(Einv->data, y, tmp_var_p2, Einv->n);
    cudaThreadSynchronize();

    // cond_den = norm_sq(tmp_var_p, p);
    status = cublasDnrm2(*handle, p, tmp_var_p, 1, &cond_den);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    // mat_vec_mult_diag(Einv, v, tmp_var_p);
    mat_vect_mult_diag_cuda<<<1, Einv->n>>>(Einv->data, v, tmp_var_p, Einv->n);
    cudaThreadSynchronize();

    double cond_den1;
    status = cublasDnrm2(*handle, p, tmp_var_p, 1, &cond_den1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    cond_den = max(max(cond_den1, cond_den), 1e-8);

    cond = pow2(tol) - cond_num / cond_den;
    return cond;
}

/**
 * @brief      Cuda (Nvidia) implemetation of QPsolver. All pointers have to point into device memory. Except d, and ws
 *
 * @param      d      { Pointer to structure with pointers to device memory -- structure data}
 * @param      ws     { Pointer to structure with pointers to device memory -- structure data}
 * @param      x_out  { Pointer to preallocated device memory -- output }
 * @param      iter   The iterator
 * @param      bt     { Pointer to preallocated device memory -- input }
 */
void qp_nvidia(struct DATA *d, struct WORK_SPACE *ws, double *x_out, int *iter, double *bt)
{

    /* define data */
    int jj = 0;

    double cond = -1;

    double theta = 1;
    double theta_old = 1;

    double alpha = 1.0;
    double beta = 0.0;
    // cublas initialization
    // Create a handle for CUBLAS
    cublasStatus_t status;
    cublasHandle_t handle;

    status = cublasCreate(&handle);
#ifdef DEBUG
    printf("Creating handle ... %s\n", _cudaGetErrorEnum(status));
#endif

    // copy_vec_part((double *)d->Q1, ws->q1, 33);
    status = cublasDcopy(handle, 33, (double *)d->Q1, 1, ws->q1, 1);
#ifdef DEBUG
    printf("Copy d->Q1 ... %s\n", _cudaGetErrorEnum(status));
#endif

    cudaMemset((void *)ws->lambda, 0, d->E->n * sizeof(double));
    cudaMemset((void *)ws->v, 0, d->E->n * sizeof(double));
    cudaMemset((void *)ws->tmp_var_n_orig, 0, 1893 * sizeof(double));
    // cublasPointerMode_t pointerMode;
    // cublasGetPointerMode(handle, &pointerMode);

    // mat_vec_mult_full(d->Q2, bt, ws->q2);
    status =
        cublasDgemv(handle, CUBLAS_OP_T, d->Q2->m, d->Q2->n, &alpha, d->Q2->data, d->Q2->m, bt, 1, &beta, ws->q2, 1);
#ifdef DEBUG
    printf("mat vect mult d->Q2*bt --> ws->q2 ... %s\n", _cudaGetErrorEnum(status));
#endif
    // vec_add(ws->q1, ws->q2, ws->q, 33);
    cudaMemset((void *)ws->q, 0, 33 * sizeof(double));
    status = cublasDaxpy(handle, 33, &alpha, ws->q1, 1, ws->q, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    status = cublasDaxpy(handle, 33, &alpha, ws->q2, 1, ws->q, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    // copy_vec_part((double *)d->Lb, ws->l, 226);
    status = cublasDcopy(handle, 226, (double *)d->Lb, 1, ws->l, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    //    copy_vec_part((double *)d->Ub, ws->u, 226);
    status = cublasDcopy(handle, 226, (double *)d->Ub, 1, ws->u, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    //    copy_vec_part((double *)d->L1, ws->tmp_var_p, 226);
    status = cublasDcopy(handle, 226, (double *)d->L1, 1, ws->tmp_var_p, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    // mat_vec_mult_full(d->L2, bt, ws->tmp_var_p2);
    status = cublasDgemv(handle, CUBLAS_OP_T, d->L2->m, d->L2->n, &alpha, d->L2->data, d->L2->m, bt, 1, &beta,
                         ws->tmp_var_p2, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    //    vec_add(ws->tmp_var_p, ws->tmp_var_p2, ws->shift_arg, 226);
    cudaMemset((void *)ws->shift_arg, 0, 226 * sizeof(double));
    status = cublasDaxpy(handle, 226, &alpha, ws->tmp_var_p, 1, ws->shift_arg, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    status = cublasDaxpy(handle, 226, &alpha, ws->tmp_var_p2, 1, ws->shift_arg, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    // mat_vec_mult_diag(d->E, ws->shift_arg, ws->Eshift_arg);
    mat_vect_mult_diag_cuda<<<1, 226>>>(d->E->data, ws->shift_arg, ws->Eshift_arg, d->E->n);
    cudaThreadSynchronize();

    // mat_vec_mult_full(d->r2, bt, ws->tmp_var_n2_orig);
    status = cublasDgemv(handle, CUBLAS_OP_T, d->r2->m, d->r2->n, &alpha, d->r2->data, d->r2->m, bt, 1, &beta,
                         ws->tmp_var_n2_orig, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    //    vec_add(ws->tmp_var_n_orig, ws->tmp_var_n2_orig, ws->r, 1893);
    cudaMemset((void *)ws->r, 0, 1893 * sizeof(double));
    status = cublasDaxpy(handle, 1893, &alpha, ws->tmp_var_n_orig, 1, ws->r, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    status = cublasDaxpy(handle, 1893, &alpha, ws->tmp_var_n2_orig, 1, ws->r, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    while ((jj < 1000) && (cond < 0)) {

        jj++;

        // mat_vec_mult_full(d->M, ws->v, ws->tmp_var_n);
        status = cublasDgemv(handle, CUBLAS_OP_T, d->M->m, d->M->n, &alpha, d->M->data, d->M->m, ws->v, 1, &beta,
                             ws->tmp_var_n, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        /////////////////////////////////////////////////////////////////////////////////////////////
        double *xPrintOut = (double *)calloc(33, sizeof(double));
        cudaMemcpy(xPrintOut, ws->tmp_var_n, 33 * sizeof(double), cudaMemcpyDeviceToHost);
        int i;

        // for (i = 0; i < 33; ++i) {
        abs(xPrintOut[10]) < 1e10 ? printf("\n|%3.3f|\n", xPrintOut[10]) : printf(".");
        // }
        // printf("\n");

        free(xPrintOut);
        /////////////////////////////////////////////////////////////////////////////////////////////

        // vec_add(ws->tmp_var_n, ws->q, ws->x, 33);
        cudaMemset((void *)ws->x, 0, 33 * sizeof(double));
        status = cublasDaxpy(handle, 33, &alpha, ws->tmp_var_n, 1, ws->x, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));
        status = cublasDaxpy(handle, 33, &alpha, ws->q, 1, ws->x, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // mat_vec_mult_full(d->C, ws->x, ws->tmp_var_p);
        status = cublasDgemv(handle, CUBLAS_OP_T, d->C->m, d->C->n, &alpha, d->C->data, d->C->m, ws->x, 1, &beta,
                             ws->tmp_var_p, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // vec_add(ws->v, ws->tmp_var_p, ws->tmp_var_p, 226);
        status = cublasDaxpy(handle, 226, &alpha, ws->v, 1, ws->tmp_var_p, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // copy_vec_part(ws->tmp_var_p, ws->arg_prox_h, 226);
        status = cublasDcopy(handle, 226, (double *)ws->tmp_var_p, 1, ws->arg_prox_h, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // vec_add(ws->tmp_var_p, ws->Eshift_arg, ws->tmp_var_p, 226);
        status = cublasDaxpy(handle, 226, &alpha, ws->Eshift_arg, 1, ws->tmp_var_p, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // TODO: write cuda implementation
        clip_soft_cuda<<<1, 226>>>(ws->tmp_var_p, ws->l, ws->u, (double *)d->soft, 226);
        cudaThreadSynchronize();

        // mat_vec_mult_diag(d->Einv, ws->tmp_var_p, ws->y);
        mat_vect_mult_diag_cuda<<<1, 226>>>(d->Einv->data, ws->tmp_var_p, ws->y, d->Einv->n);
        cudaThreadSynchronize();

        // vec_sub(ws->y, ws->shift_arg, ws->y, 226);
        alpha = -1;
        status = cublasDaxpy(handle, 226, &alpha, ws->shift_arg, 1, ws->y, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // mat_vec_mult_diag(d->E, ws->y, ws->tmp_var_p);
        mat_vect_mult_diag_cuda<<<1, 226>>>(d->E->data, ws->y, ws->tmp_var_p, d->E->n);
        cudaThreadSynchronize();

        // copy_vec_part(ws->lambda, ws->lambda_old, 226);
        status = cublasDcopy(handle, 226, (double *)ws->lambda, 1, ws->lambda_old, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // vec_sub(ws->arg_prox_h, ws->tmp_var_p, ws->lambda, 226);
        cudaMemset((void *)ws->lambda, 0, 226 * sizeof(double));
        status = cublasDaxpy(handle, 226, &alpha, ws->tmp_var_p, 1, ws->lambda, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));
        alpha = 1;
        status = cublasDaxpy(handle, 226, &alpha, ws->arg_prox_h, 1, ws->lambda, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // vec_sub(ws->lambda, ws->lambda_old, ws->tmp_var_p, 226);
        cudaMemset((void *)ws->tmp_var_p, 0, 226 * sizeof(double));
        alpha = -1;
        status = cublasDaxpy(handle, 226, &alpha, ws->lambda_old, 1, ws->tmp_var_p, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));
        alpha = 1;
        status = cublasDaxpy(handle, 226, &alpha, ws->lambda, 1, ws->tmp_var_p, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        theta_old = theta;
        theta = (1 + sqrt(1 + 4 * pow(theta_old, 2))) / 2;

        // scalar_mult((theta_old - 1) / theta, ws->tmp_var_p, 226);
        beta = (theta_old - 1) / theta;
        status = cublasDscal(handle, 226, &beta, ws->tmp_var_p, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));
        beta = 0.0;

        // copy_vec_part(ws->v, ws->v_old, 226);
        status = cublasDcopy(handle, 226, (double *)ws->v, 1, ws->v_old, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        // vec_add(ws->tmp_var_p, ws->lambda, ws->v, 226);
        cudaMemset((void *)ws->v, 0, 226 * sizeof(double));
        status = cublasDaxpy(handle, 226, &alpha, ws->tmp_var_p, 1, ws->v, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));
        status = cublasDaxpy(handle, 226, &alpha, ws->lambda, 1, ws->v, 1);
        if (status != CUBLAS_STATUS_SUCCESS)
            printf("%s\n", _cudaGetErrorEnum(status));

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Set fixed number of iterations
        //
        if (mod(jj, 10) == 0) {

            cond = check_stop_cond_FGM(&handle, d->Einv, ws->lambda, ws->lambda_old, ws->tmp_var_p, ws->tmp_var_p2, 226,
                                       0.001);
        }

        restart(&handle, ws->lambda, ws->lambda_old, ws->v, ws->v_old, ws->tmp_var_p, ws->tmp_var_p2, 226);
    }

    // mat_vec_mult_full(d->R,ws->x,ws->tmp_var_n_orig);
    status = cublasDgemv(handle, CUBLAS_OP_T, d->R->m, d->R->n, &alpha, d->R->data, d->R->m, ws->x, 1, &beta,
                         ws->tmp_var_n_orig, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));

    // vec_add(ws->tmp_var_n_orig, ws->r, x_out, 1893);
    cudaMemset((void *)ws->tmp_var_n2_orig, 0, 1893 * sizeof(double));
    status = cublasDaxpy(handle, 1893, &alpha, ws->tmp_var_n_orig, 1, ws->tmp_var_n2_orig, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    status = cublasDaxpy(handle, 1893, &alpha, ws->r, 1, ws->tmp_var_n2_orig, 1);
    if (status != CUBLAS_STATUS_SUCCESS)
        printf("%s\n", _cudaGetErrorEnum(status));
    cudaMemcpy(x_out, ws->tmp_var_n2_orig, sizeof(double) * 1893, cudaMemcpyDeviceToHost);
    cublasDestroy(handle);
    *iter = jj;
}
