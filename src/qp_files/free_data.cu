#include <stdlib.h>
#include "QPgen.h"
#include "data_struct.h"

 void free_data(struct DATA *d) {

cudaFree(d->M->data);

cudaFree(d->M);

cudaFree(d->Q1);

cudaFree(d->Q2->data);

cudaFree(d->Q2);

cudaFree(d->C->data);

cudaFree(d->C);

cudaFree(d->CT->data);

cudaFree(d->CT);

cudaFree(d->E->data);

cudaFree(d->E);

cudaFree(d->Einv->data);

cudaFree(d->Einv);

cudaFree(d->Lb);

cudaFree(d->Ub);

cudaFree(d->soft);

cudaFree(d->L1);

cudaFree(d->L2->data);

cudaFree(d->L2);

cudaFree(d->r2->data);

cudaFree(d->r2);

cudaFree(d->R->data);

cudaFree(d->R);

cudaFree(d);

}

