#include <stdlib.h>
#include "work_space_struct.h"

void free_work_space(struct WORK_SPACE *ws) {

cudaFree(ws->q);

cudaFree(ws->q1);

cudaFree(ws->q2);

cudaFree(ws->l);

cudaFree(ws->s);

cudaFree(ws->u);

cudaFree(ws->shift_arg);

cudaFree(ws->Eshift_arg);

cudaFree(ws->tmp_var_p);

cudaFree(ws->tmp_var_p2);

cudaFree(ws->arg_prox_h);

cudaFree(ws->tmp_var_n_orig);

cudaFree(ws->tmp_var_n2_orig);

cudaFree(ws->r);

cudaFree(ws->lambda);

cudaFree(ws->y);

cudaFree(ws->x);

cudaFree(ws->lambda_old);

cudaFree(ws->v);

cudaFree(ws->v_old);

cudaFree(ws->tmp_var_n);

cudaFree(ws->tmp_var_n2);

cudaFree(ws);

}

