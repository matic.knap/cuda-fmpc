#include <stdlib.h>
#include "work_space_struct.h"

void init_work_space(struct WORK_SPACE *ws) {

double *q ;
cudaMalloc(&q, 33*sizeof(double));

ws->q = q;

double *q1 ;
cudaMalloc(&q1, 33*sizeof(double));

ws->q1 = q1;

double *q2;
cudaMalloc(&q2, 33*sizeof(double));

ws->q2 = q2;

double *l;
cudaMalloc(&l, 226*sizeof(double));

ws->l = l;

double *s ;
cudaMalloc(&s, 226*sizeof(double));

ws->s = s;

double *u ;
cudaMalloc(&u, 226*sizeof(double));

ws->u = u;

double *shift_arg ;
cudaMalloc(&shift_arg, 226*sizeof(double));

ws->shift_arg = shift_arg;

double *Eshift_arg ;
cudaMalloc(&Eshift_arg, 226*sizeof(double));

ws->Eshift_arg = Eshift_arg;

double *tmp_var_p ;
cudaMalloc(&tmp_var_p, 226*sizeof(double));

ws->tmp_var_p = tmp_var_p;

double *tmp_var_p2 ;
cudaMalloc(&tmp_var_p2, 226*sizeof(double));

ws->tmp_var_p2 = tmp_var_p2;

double *arg_prox_h ;
cudaMalloc(&arg_prox_h, 226*sizeof(double));

ws->arg_prox_h = arg_prox_h;

double *tmp_var_n_orig ;
cudaMalloc(&tmp_var_n_orig, 1893*sizeof(double));

ws->tmp_var_n_orig = tmp_var_n_orig;

double *tmp_var_n2_orig ;
cudaMalloc(&tmp_var_n2_orig, 1893*sizeof(double));

ws->tmp_var_n2_orig = tmp_var_n2_orig;

double *r ;
cudaMalloc(&r, 1893*sizeof(double));

ws->r = r;

double *lambda ;
cudaMalloc(&lambda, 226*sizeof(double));

ws->lambda = lambda;

double *y ;
cudaMalloc(&y, 226*sizeof(double));

ws->y = y;

double *x ;
cudaMalloc(&x, 33*sizeof(double));

ws->x = x;

double *lambda_old ;
cudaMalloc(&lambda_old, 226*sizeof(double));

ws->lambda_old = lambda_old;

double *v ;
cudaMalloc(&v, 226*sizeof(double));

ws->v = v;

double *v_old ;
cudaMalloc(&v_old, 226*sizeof(double));

ws->v_old = v_old;

double *tmp_var_n ;
cudaMalloc(&tmp_var_n, 33*sizeof(double));

ws->tmp_var_n = tmp_var_n;

double *tmp_var_n2 ;
cudaMalloc(&tmp_var_n2, 33*sizeof(double));

ws->tmp_var_n2 = tmp_var_n2;

}

