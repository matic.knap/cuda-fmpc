#ifndef DATA_STRUCT_H_GUARD
#define DATA_STRUCT_H_GUARD

struct DATA {
struct FULL_MAT *M;
double *Q1;
struct FULL_MAT *Q2;
struct FULL_MAT *C;
struct FULL_MAT *CT;
struct DIAG_MAT *E;
struct DIAG_MAT *Einv;
double *Lb;
double *Ub;
double *soft;
double *L1;
struct FULL_MAT *L2;
struct FULL_MAT *r2;
struct FULL_MAT *R;
};

#endif

