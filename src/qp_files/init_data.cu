#include "QPgen.h"
#include "data_struct.h"
#include <stdio.h>
#include <stdlib.h>

static void read_binary_double(double *data, size_t len, char *file_name)
{
    FILE *fp = fopen(file_name, "rb");
    if (!fp) {
        perror("File open error");
    }
    fread(data, sizeof(double), len, fp);
    fclose(fp);
}

static void read_binary_int(int *data, size_t len, char *file_name)
{
    FILE *fp = fopen(file_name, "rb");
    if (!fp) {
        perror("File open error");
    }
    fread(data, sizeof(int), len, fp);
    fclose(fp);
}

static void read_binary_float(float *data, size_t len, char *file_name)
{
    FILE *fp = fopen(file_name, "rb");
    if (!fp) {
        perror("File open error");
    }
    fread(data, sizeof(float), len, fp);
    fclose(fp);
}

void init_data(struct DATA *d)
{

    double *Mdata = (double *)calloc(7458, sizeof(double));
    read_binary_double(Mdata, 7458, "/home/fmpc/mknap/cuda-fmpc/qp_data/Mdata.bin");

    struct FULL_MAT *M = (struct FULL_MAT *)calloc(1, sizeof(struct FULL_MAT));
    M->n = 33;
    M->m = 226;
   
    // move data to GPU
    cudaMalloc(&M->data, 7458 * sizeof(double));
    cudaMemcpy(M->data, Mdata, 7458 * sizeof(double), cudaMemcpyHostToDevice);

    d->M = M;
    free(Mdata);

    double *Q1 = (double *)calloc(33, sizeof(double));
    read_binary_double(Q1, 33, "/home/fmpc/mknap/cuda-fmpc/qp_data/Q1.bin");

    // move data to GPU
    cudaMalloc(&d->Q1, 33 * sizeof(double));
    cudaMemcpy(d->Q1, Q1, 33 * sizeof(double), cudaMemcpyHostToDevice);

    free(Q1);

    double *Q2data = (double *)calloc(2046, sizeof(double));

    read_binary_double(Q2data, 2046, "/home/fmpc/mknap/cuda-fmpc/qp_data/Q2data.bin");

    struct FULL_MAT *Q2 = (struct FULL_MAT *)calloc(1, sizeof(struct FULL_MAT));

    Q2->n = 33;

    Q2->m = 62;

    // move data to GPU
    cudaMalloc(&Q2->data, 7458 * sizeof(double));
    cudaMemcpy(Q2->data, Q2data, 7458 * sizeof(double), cudaMemcpyHostToDevice);
    d->Q2 = Q2;


    double *Cdata = (double *)calloc(7458, sizeof(double));;
    read_binary_double(Cdata, 7458, "/home/fmpc/mknap/cuda-fmpc/qp_data/Cdata.bin");

    struct FULL_MAT *C = (struct FULL_MAT *)calloc(1, sizeof(struct FULL_MAT));
    C->n = 226;
    C->m = 33;

    // move data to GPU
    cudaMalloc(&C->data, 7458 * sizeof(double));
    cudaMemcpy(C->data, Cdata, 7458 * sizeof(double), cudaMemcpyHostToDevice);

    d->C = C;

    double *CTdata = (double *)calloc(7458, sizeof(double));

    read_binary_double(CTdata, 7458, "/home/fmpc/mknap/cuda-fmpc/qp_data/CTdata.bin");

    struct FULL_MAT *CT = (struct FULL_MAT *)calloc(1, sizeof(struct FULL_MAT));

    CT->n = 33;

    CT->m = 226;

    // move data to GPU
    cudaMalloc(&CT->data, 7458 * sizeof(double));
    cudaMemcpy(CT->data, CTdata, 7458 * sizeof(double), cudaMemcpyHostToDevice);

    d->CT = CT;

    free(CTdata);

    double *Edata = (double *)calloc(226, sizeof(double));

    read_binary_double(Edata, 226, "/home/fmpc/mknap/cuda-fmpc/qp_data/Edata.bin");

    struct DIAG_MAT *E= (struct DIAG_MAT *)calloc(1, sizeof(struct DIAG_MAT));

    E->n = 226;

    // move data to GPU
    cudaMalloc(&E->data, 226 * sizeof(double));
    cudaMemcpy(E->data, Edata, 226 * sizeof(double), cudaMemcpyHostToDevice);

    d->E = E;    
    free(Edata);


    double *Einvdata = (double *)calloc(226, sizeof(double));

    read_binary_double(Einvdata, 226, "/home/fmpc/mknap/cuda-fmpc/qp_data/Einvdata.bin");

    struct DIAG_MAT *Einv= (struct DIAG_MAT *)calloc(1, sizeof(struct DIAG_MAT));
    Einv->n = 226;

    // move data to GPU
    cudaMalloc(&Einv->data, 226 * sizeof(double));
    cudaMemcpy(Einv->data, Einvdata, 226 * sizeof(double), cudaMemcpyHostToDevice);

    d->Einv = Einv;

    free(Einvdata);


    double *Lb = (double *)calloc(226, sizeof(double));

    read_binary_double(Lb, 226, "/home/fmpc/mknap/cuda-fmpc/qp_data/Lb.bin");

    d->Lb = Lb;

    // move data to GPU
    cudaMalloc(&d->Lb, 33 * sizeof(double));
    cudaMemcpy(d->Lb, Lb, 33 * sizeof(double), cudaMemcpyHostToDevice);

    free(Lb);

    double *Ub = (double *)calloc(226, sizeof(double));

    read_binary_double(Ub, 226, "/home/fmpc/mknap/cuda-fmpc/qp_data/Ub.bin");

    d->Ub = Ub;

    // move data to GPU
    cudaMalloc(&d->Ub, 33 * sizeof(double));
    cudaMemcpy(d->Ub, Ub, 33 * sizeof(double), cudaMemcpyHostToDevice);

    free(Ub);

    
    double *soft = (double *)calloc(226, sizeof(double));

    read_binary_double(soft, 226, "/home/fmpc/mknap/cuda-fmpc/qp_data/soft.bin");

    d->soft = soft;

    // move data to GPU
    cudaMalloc(&d->soft, 226 * sizeof(double));
    cudaMemcpy(d->soft, soft, 226 * sizeof(double), cudaMemcpyHostToDevice);

    free(soft);

    double *L1 = (double *)calloc(226, sizeof(double));

    read_binary_double(L1, 226, "/home/fmpc/mknap/cuda-fmpc/qp_data/L1.bin");

    d->L1 = L1;

    // move data to GPU
    cudaMalloc(&d->L1, 33 * sizeof(double));
    cudaMemcpy(d->L1, L1, 33 * sizeof(double), cudaMemcpyHostToDevice);

    free(L1);

    double *L2data = (double *)calloc(14012, sizeof(double));

    read_binary_double(L2data, 14012, "/home/fmpc/mknap/cuda-fmpc/qp_data/L2data.bin");

    struct FULL_MAT *L2 = (struct FULL_MAT *)calloc(1, sizeof(struct FULL_MAT));

    L2->n = 226;

    L2->m = 62;

    // move data to GPU
    cudaMalloc(&L2->data, 14012 * sizeof(double));
    cudaMemcpy(L2->data, L2data, 14012 * sizeof(double), cudaMemcpyHostToDevice);

    d->L2 = L2;

    free(L2data);

    double *r2data = (double *)calloc(117336, sizeof(double));

    read_binary_double(r2data, 117366, "/home/fmpc/mknap/cuda-fmpc/qp_data/r2data.bin");

    struct FULL_MAT *r2 = (struct FULL_MAT *)calloc(1, sizeof(struct FULL_MAT));


    r2->n = 1893;

    r2->m = 62;

    // move data to GPU
    cudaMalloc(&r2->data, 117336 * sizeof(double));
    cudaMemcpy(r2->data, r2data, 117336 * sizeof(double), cudaMemcpyHostToDevice);

    d->r2 = r2;

    free(r2data);

    double *Rdata = (double *)calloc(62469, sizeof(double));

    read_binary_double(Rdata, 62469, "/home/fmpc/mknap/cuda-fmpc/qp_data/Rdata.bin");

    struct FULL_MAT *R = (struct FULL_MAT *)calloc(1, sizeof(struct FULL_MAT)) ;

    R->n = 1893;
    R->m = 33;

    // move data to GPU
    cudaMalloc(&R->data, 62469 * sizeof(double));
    cudaMemcpy(R->data, Rdata, 62469 * sizeof(double), cudaMemcpyHostToDevice);

    d->R = R;
    free(Rdata);
}
