#include "mex.h"
#include "QPgen.h"
#include "data_struct.h"
#include "work_space_struct.h"


 void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

 /* define variables */
 double *x;
 int *iter;
 double *s;
double *bt;

/* check inputs */

 if (!(nrhs == 1))
 {
 mexErrMsgTxt("Wrong nbr of inputs");
} 

bt = mxGetPr(prhs[0]);

if (!IS_REAL_2D_FULL_DOUBLE_VECTOR(prhs[0])  || (mxGetM(prhs[0]) != 62)){
 mexErrMsgTxt("Input 1 should be real full vector of size (62,1)");
}

/* set output */
 plhs[0] = mxCreateDoubleMatrix(1893,1,mxREAL);

x = mxGetPr(plhs[0]);

 /* run main loop */
plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);

iter = (int *) mxGetData(plhs[1]);

/* set output */
 plhs[2] = mxCreateDoubleMatrix(226,1, mxREAL);

s = mxGetPr(plhs[2]);

 /* run main loop */
struct DATA *d = malloc(sizeof(struct DATA));

init_data(d);

struct WORK_SPACE *ws = malloc(sizeof(struct WORK_SPACE));

init_work_space(ws);

qp(d,ws,x,iter,bt);

free_data(d);

free_work_space(ws);

}